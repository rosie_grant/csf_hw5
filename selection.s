# Assignment 5
# rgrant8@jhu.edu
# selection sort an array read from stdin to stdout
#
# we read up to 128 positive integers from stdin;
# if there are fewer than 128 integers, a negative
# integer can be used to mark the end of the input;
# we sort the array using selection sort and finally
# print the sorted array back to stdout

sys_print_int = 1
sys_print_string = 4
sys_read_int = 5

SLOTS = 128	# at most 128 values
BYTES = 512	# need 128*4 bytes of space

	.text
main:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	li	$a0, SLOTS
	la	$a1, buffer
	jal	read_array
	ble	$v0, $zero, exit 
	sw	$v0, used 

	lw	$a0, used # a0 = n 
	la	$a1, buffer # a1 = address of array
	jal	selection_sort

	lw	$a0, used	
	la	$a1, buffer
	jal	print_array
exit:
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4

	jr	$ra

# length = read_array(size, address)
# $v0                 $a0   $a1
read_array:
	addi	$sp, $sp, -16	# save used registers
	sw	$s2, 12($sp)
	sw	$s1, 8($sp)
	sw	$s0, 4($sp)
	sw	$ra, 0($sp)

	move	$s0, $zero		# slots used
	move	$s1, $a1		# address in array
	move	$s2, $a0		# slots available
ra_loop:
	li	$v0, sys_read_int	# read next value
	syscall

	blt	$v0, $zero, ra_done	# break if we read < 0

	sw	$v0, ($s1)		# store value in array
	addi	$s0, $s0, 1		# used++
	addi	$s1, $s1, 4		# address++

	blt	$s0, $s2, ra_loop	# while used < available
ra_done:
	move	$v0, $s0		# return slots used

	lw	$ra, 0($sp)		# restore used registers
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	addi	$sp, $sp, 16

	jr	$ra			# return

# selection_sort(size, address)
#             $a0   $a1
selection_sort:
	addi	$sp, $sp, -16	# Store registers
	sw	$s2, 12($sp)
	sw	$s1, 8($sp)
	sw	$s0, 4($sp)
	sw	$ra, 0($sp)

	move $s0, $a0 		# s0 = num elements = n
	li $t0, 1
	sub $s1, $s0, $t0	# Num elements - 1
	li $s2, 0			# $s2 = outercounter = i
	
	outer_loop:
	bge $s2, $s1, outer_ex	# If i >= n - 1, exit
	move $s4, $s2			# k = i = min_index, 1st element
	addi $s3, $s2, 1		# j = i + 1
		
		inner_loop:
		bgt $s3, $s1, inner_ex	# if j >= n - 1, exit inner loop
			
		sll $t0, $s4, 2			# k * 4
		add $t0, $t0, $a1		# address of min element
			
		sll $t1, $s3, 2 		# j * 4
		add $t1, $t1, $a1		# address of curr element
			
		lw $t2, 0($t0)			# array[k], min element
		lw $t3, 0($t1)			# array[j], curr element
			
		bge $t3, $t2, hop		# if array[k] < array[j], update min index
		move $s4, $s3			# k = j
			
		hop:
		addi $s3, $s3, 1		# j++
		j inner_loop
		
		inner_ex:
		move $a2, $s2
		move $a3, $s4  
		jal swap				# jump to swap routine 

		back:
		addi $s2, $s2, 1 		# i++
		j outer_loop
	outer_ex:
		#lw 	$ra, 0($sp)		#restore ra
		#addi	$sp, $sp, 4

		lw	$ra, 0($sp)		# restore registers & ra
		lw	$s0, 4($sp)
		lw	$s1, 8($sp)
		lw	$s2, 12($sp)
		addi	$sp, $sp, 16
		jr $ra
		
# swap(array address, element a, element b)
#		$a1			   $a2		   $a3
# element a: leftmost unsorted
# element b: new min
swap:
	sll $t0, $a2, 2		# i * 4
	add $t0, $a1, $t0	# leftmost unsorted element address            
	
	sll $t1, $a3, 2		# k * 4
	add $t1, $a1, $t1	# address of min element
	
	lw $t2, 0($t0)		# array[i]
	lw $t3, 0($t1)		# array[k]
	sw $t3, 0($t0)		# array[i] = array[k] = new min
	sw $t2, 0($t1)		# array[k] = old min
	
	jr $ra	
	
# print_array(size, address)
#             $a0   $a1
print_array:
	addi	$sp, $sp, -16		# save used registers
	sw	$s2, 12($sp)
	sw	$s1, 8($sp)
	sw	$s0, 4($sp)
	sw	$ra, 0($sp)

	move	$s0, $zero		# current slot
	move	$s1, $a1		# address in array
	move	$s2, $a0		# slots used
pa_loop:
	bge	$s0, $s2, pa_done	# while current < used

	lw	$a0, ($s1)		# load value in array
	jal	print_int		# and print it

	addi	$s0, $s0, 1			# current++
	addi	$s1, $s1, 4			# address++

	b	pa_loop
pa_done:
	lw	$ra, 0($sp)		# restore used registers
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	addi	$sp, $sp, 16

	jr	$ra			# return

# print_int(value)
#           $a0
print_int:
	li	$v0, sys_print_int
	syscall

	la	$a0, lf
	li	$v0, sys_print_string
	syscall

	jr	$ra

# print_string(string)
#              $a0
print_string:
	li	$v0, sys_print_string
	syscall

	la	$a0, lf
	li	$v0, sys_print_string
	syscall

	jr	$ra

	.data
buffer:
	.space	BYTES		# array of 4-byte integers
used:
	.word	0		# used slots in array
lf:
	.asciiz	"\n"

	.end
