# Assignment 5
# rgrant8@jhu.edu
# Iterative fibonacci function.

print_int = 1
print_string = 4
read_int = 5

	.text
# Note that for main() we make no assumption about the
# caller having set aside memory for saving arguments.
# Luckily, in this case, we don't need to save them.
main:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	li	$v0, read_int	# Reads an int from standard input
	syscall

	move	$a0, $v0	# Input is in $a0
	jal	fib

	move	$a0, $v0	# Result stored in $v0
	li	$v0, print_int
	syscall

	la	$a0, lf			# Print
	li	$v0, print_string
	syscall

	lw	$ra, ($sp) 		# Restore sp
	addi	$sp, $sp, 4

	jr	$ra

fib:
	li $v0, 0	# next
	li $t0, 2	# i
	li $t1, 0	# prev
	li $t2, 1	# curr
	
	ble  $a0, 1, base_case # n == 0 or 1
	
loop:
	bgt $t0, $a0, done 	# i > n
	add $v0, $t1, $t2	# next = prev + curr
	move $t1, $t2		# prev = curr
	move $t2, $v0		# curr = next
	addi $t0, $t0, 1	# i++
	j	loop
base_case:
	move $v0, $a0 	# Oth fib = 0, 1st fib = 1
done:
	jr	$ra

	.data
lf:
	.asciiz	"\n"
	.end
