# Assignment 5
# rgrant8@jhu.edu
# Iterative factorial function.

print_int = 1
print_string = 4
read_int = 5

	.text
# Note that for main() we make no assumption about the
# caller having set aside memory for saving arguments.
# Luckily, in this case, we don't need to save them.
main:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	li	$v0, read_int	# Reads an int from standard input
	syscall

	move	$a0, $v0	# Input is in $a0
	jal	isqrt

	move	$a0, $v0	# Result stored in $v0
	li	$v0, print_int
	syscall

	la	$a0, lf			# Print
	li	$v0, print_string
	syscall

	lw	$ra, ($sp)		# Restore sp
	addi	$sp, $sp, 4

	jr	$ra

isqrt:
	#a0 has n
	li $t0, 0	# i
	srl $t1, $a0, 1 	# $t1 = n/2
	
	move $v0, $a0 		# $v0 = x, and x = n init
loop:
	bge $t0, $t1, done	# Contineu so long as i < n/2
	div $a0, $v0		# $t2 = n/x
	mflo $t2
	add $t2, $t2, $v0	# $t2 = x + (n/x)
	srl $v0, $t2, 1		# $v0 = x = (x + n/x) / 2
	addi $t0, $t0, 1	# i++
	j loop
done:
	jr	$ra

	.data
lf:
	.asciiz	"\n"
	.end
