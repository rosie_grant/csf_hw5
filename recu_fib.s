# Assignment 5
# rgrant8@jhu.edu
# Recursive fibonacci function.

print_int = 1
print_string = 4
read_int = 5

	.text
# Note that for main() we make no assumption about the
# caller having set aside memory for saving arguments.
# Luckily, in this case, we don't need to save them.
main:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	li	$v0, read_int	 # Reads an int from standard input
	syscall

	move	$a0, $v0	# Input is in $a0
	jal	fib

	move	$a0, $v0	# Result stored in $v0
	li	$v0, print_int
	syscall

	la	$a0, lf			# Print
	li	$v0, print_string
	syscall

	lw	$ra, 0($sp)		# Restore sp
	addi	$sp, $sp, 4

	jr	$ra

# This version uses the memory the caller set aside
# to save $a0 across the recursive calls.
fib:

	ble $a0, 1 , base_case 	# Stop at 1 and 0
	
	addi $sp, $sp, -16 	# Decrement sp by 16
	sw	$ra, 0($sp)		# 0(sp) == return address
	sw	$a0, 4($sp)		# 4(sp) == n
	
	addi $a0, $a0, -1 	# n = n - 1
	jal	fib
	sw $v0, 8($sp)		# Store fib(n-1) at 8(sp)
	lw $a0, 4($sp)		# Restore n
	
	addi $a0, $a0, -2	# n = n - 2
	jal fib
	sw $v0, 12($sp)		# Store fib(n-2) at 12(sp)
	
	b	done
	
base_case:
	move	$v0, $a0	# Answer is 1 or 0
	jr $ra
	
done:
	lw	$t0, 12($sp)		# fib(n-2)
	lw 	$t1, 8($sp)			# fib(n-1)
	lw	$ra, 0($sp)			# Restore return address
	add $sp, $sp, 16		# Restore stack pointer
	
	add $v0, $t0, $t1		# fib(n) = fib(n-2) + fib(n-1)

	jr	$ra

	.data
lf:
	.asciiz	"\n"
	.end
